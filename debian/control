Source: python-crypto
Section: python
Priority: optional
Maintainer: Sebastian Ramacher <sramacher@debian.org>
Build-Depends:
 debhelper-compat (= 12),
 dh-python,
 libgmp-dev,
 python3-all-dbg,
 python3-all-dev (>= 3.3.2-5~)
Standards-Version: 4.5.0
Vcs-Git: https://salsa.debian.org/sramacher/python-crypto.git
Vcs-Browser: https://salsa.debian.org/sramacher/python-crypto
Homepage: http://www.pycrypto.org/
Rules-Requires-Root: no

Package: python3-crypto
Architecture: any
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends}
Provides:
 ${python3:Provides}
Description: cryptographic algorithms and protocols for Python 3
 A collection of cryptographic algorithms and protocols, implemented
 for use from Python 3. Among the contents of the package:
 .
  * Hash functions: HMAC, MD2, MD4, MD5, RIPEMD160, SHA, SHA256.
  * Block encryption algorithms: AES, ARC2, Blowfish, CAST, DES, Triple-DES.
  * Stream encryption algorithms: ARC4, simple XOR.
  * Public-key algorithms: RSA, DSA, ElGamal.
  * Protocols: All-or-nothing transforms, chaffing/winnowing.
  * Miscellaneous: RFC1751 module for converting 128-bit keys
    into a set of English words, primality testing, random number generation.

Package: python3-crypto-dbg
Section: debug
Architecture: any
Depends:
 python3-crypto (= ${binary:Version}),
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends}
Provides:
 ${python3:Provides}
Description: cryptographic algorithms and protocols for Python 3 (debug extension)
 A collection of cryptographic algorithms and protocols, implemented
 for use from Python 3.
 .
 This package contains the extensions built for the Python 3 debug interpreter.
